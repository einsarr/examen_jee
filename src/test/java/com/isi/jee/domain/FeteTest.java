package com.isi.jee.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.isi.jee.web.rest.TestUtil;

public class FeteTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Fete.class);
        Fete fete1 = new Fete();
        fete1.setId(1L);
        Fete fete2 = new Fete();
        fete2.setId(fete1.getId());
        assertThat(fete1).isEqualTo(fete2);
        fete2.setId(2L);
        assertThat(fete1).isNotEqualTo(fete2);
        fete1.setId(null);
        assertThat(fete1).isNotEqualTo(fete2);
    }
}
