package com.isi.jee.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.isi.jee.web.rest.TestUtil;

public class TypelieuTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Typelieu.class);
        Typelieu typelieu1 = new Typelieu();
        typelieu1.setId(1L);
        Typelieu typelieu2 = new Typelieu();
        typelieu2.setId(typelieu1.getId());
        assertThat(typelieu1).isEqualTo(typelieu2);
        typelieu2.setId(2L);
        assertThat(typelieu1).isNotEqualTo(typelieu2);
        typelieu1.setId(null);
        assertThat(typelieu1).isNotEqualTo(typelieu2);
    }
}
