package com.isi.jee.web.rest;

import com.isi.jee.ExamenApp;
import com.isi.jee.domain.Fete;
import com.isi.jee.repository.FeteRepository;
import com.isi.jee.service.FeteService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FeteResource} REST controller.
 */
@SpringBootTest(classes = ExamenApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FeteResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_ETAT = "AAAAAAAAAA";
    private static final String UPDATED_ETAT = "BBBBBBBBBB";

    @Autowired
    private FeteRepository feteRepository;

    @Autowired
    private FeteService feteService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFeteMockMvc;

    private Fete fete;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fete createEntity(EntityManager em) {
        Fete fete = new Fete()
            .libelle(DEFAULT_LIBELLE)
            .description(DEFAULT_DESCRIPTION)
            .etat(DEFAULT_ETAT);
        return fete;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fete createUpdatedEntity(EntityManager em) {
        Fete fete = new Fete()
            .libelle(UPDATED_LIBELLE)
            .description(UPDATED_DESCRIPTION)
            .etat(UPDATED_ETAT);
        return fete;
    }

    @BeforeEach
    public void initTest() {
        fete = createEntity(em);
    }

    @Test
    @Transactional
    public void createFete() throws Exception {
        int databaseSizeBeforeCreate = feteRepository.findAll().size();
        // Create the Fete
        restFeteMockMvc.perform(post("/api/fetes").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fete)))
            .andExpect(status().isCreated());

        // Validate the Fete in the database
        List<Fete> feteList = feteRepository.findAll();
        assertThat(feteList).hasSize(databaseSizeBeforeCreate + 1);
        Fete testFete = feteList.get(feteList.size() - 1);
        assertThat(testFete.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testFete.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testFete.getEtat()).isEqualTo(DEFAULT_ETAT);
    }

    @Test
    @Transactional
    public void createFeteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = feteRepository.findAll().size();

        // Create the Fete with an existing ID
        fete.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFeteMockMvc.perform(post("/api/fetes").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fete)))
            .andExpect(status().isBadRequest());

        // Validate the Fete in the database
        List<Fete> feteList = feteRepository.findAll();
        assertThat(feteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFetes() throws Exception {
        // Initialize the database
        feteRepository.saveAndFlush(fete);

        // Get all the feteList
        restFeteMockMvc.perform(get("/api/fetes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fete.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT)));
    }
    
    @Test
    @Transactional
    public void getFete() throws Exception {
        // Initialize the database
        feteRepository.saveAndFlush(fete);

        // Get the fete
        restFeteMockMvc.perform(get("/api/fetes/{id}", fete.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fete.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.etat").value(DEFAULT_ETAT));
    }
    @Test
    @Transactional
    public void getNonExistingFete() throws Exception {
        // Get the fete
        restFeteMockMvc.perform(get("/api/fetes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFete() throws Exception {
        // Initialize the database
        feteService.save(fete);

        int databaseSizeBeforeUpdate = feteRepository.findAll().size();

        // Update the fete
        Fete updatedFete = feteRepository.findById(fete.getId()).get();
        // Disconnect from session so that the updates on updatedFete are not directly saved in db
        em.detach(updatedFete);
        updatedFete
            .libelle(UPDATED_LIBELLE)
            .description(UPDATED_DESCRIPTION)
            .etat(UPDATED_ETAT);

        restFeteMockMvc.perform(put("/api/fetes").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFete)))
            .andExpect(status().isOk());

        // Validate the Fete in the database
        List<Fete> feteList = feteRepository.findAll();
        assertThat(feteList).hasSize(databaseSizeBeforeUpdate);
        Fete testFete = feteList.get(feteList.size() - 1);
        assertThat(testFete.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testFete.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testFete.getEtat()).isEqualTo(UPDATED_ETAT);
    }

    @Test
    @Transactional
    public void updateNonExistingFete() throws Exception {
        int databaseSizeBeforeUpdate = feteRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFeteMockMvc.perform(put("/api/fetes").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fete)))
            .andExpect(status().isBadRequest());

        // Validate the Fete in the database
        List<Fete> feteList = feteRepository.findAll();
        assertThat(feteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFete() throws Exception {
        // Initialize the database
        feteService.save(fete);

        int databaseSizeBeforeDelete = feteRepository.findAll().size();

        // Delete the fete
        restFeteMockMvc.perform(delete("/api/fetes/{id}", fete.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Fete> feteList = feteRepository.findAll();
        assertThat(feteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
