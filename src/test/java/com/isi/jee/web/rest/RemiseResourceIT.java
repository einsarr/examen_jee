package com.isi.jee.web.rest;

import com.isi.jee.ExamenApp;
import com.isi.jee.domain.Remise;
import com.isi.jee.repository.RemiseRepository;
import com.isi.jee.service.RemiseService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RemiseResource} REST controller.
 */
@SpringBootTest(classes = ExamenApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class RemiseResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Float DEFAULT_POURCENTAGE = 1F;
    private static final Float UPDATED_POURCENTAGE = 2F;

    @Autowired
    private RemiseRepository remiseRepository;

    @Autowired
    private RemiseService remiseService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRemiseMockMvc;

    private Remise remise;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Remise createEntity(EntityManager em) {
        Remise remise = new Remise()
            .libelle(DEFAULT_LIBELLE)
            .description(DEFAULT_DESCRIPTION)
            .pourcentage(DEFAULT_POURCENTAGE);
        return remise;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Remise createUpdatedEntity(EntityManager em) {
        Remise remise = new Remise()
            .libelle(UPDATED_LIBELLE)
            .description(UPDATED_DESCRIPTION)
            .pourcentage(UPDATED_POURCENTAGE);
        return remise;
    }

    @BeforeEach
    public void initTest() {
        remise = createEntity(em);
    }

    @Test
    @Transactional
    public void createRemise() throws Exception {
        int databaseSizeBeforeCreate = remiseRepository.findAll().size();
        // Create the Remise
        restRemiseMockMvc.perform(post("/api/remises").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remise)))
            .andExpect(status().isCreated());

        // Validate the Remise in the database
        List<Remise> remiseList = remiseRepository.findAll();
        assertThat(remiseList).hasSize(databaseSizeBeforeCreate + 1);
        Remise testRemise = remiseList.get(remiseList.size() - 1);
        assertThat(testRemise.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testRemise.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testRemise.getPourcentage()).isEqualTo(DEFAULT_POURCENTAGE);
    }

    @Test
    @Transactional
    public void createRemiseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = remiseRepository.findAll().size();

        // Create the Remise with an existing ID
        remise.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRemiseMockMvc.perform(post("/api/remises").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remise)))
            .andExpect(status().isBadRequest());

        // Validate the Remise in the database
        List<Remise> remiseList = remiseRepository.findAll();
        assertThat(remiseList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRemises() throws Exception {
        // Initialize the database
        remiseRepository.saveAndFlush(remise);

        // Get all the remiseList
        restRemiseMockMvc.perform(get("/api/remises?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(remise.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].pourcentage").value(hasItem(DEFAULT_POURCENTAGE.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getRemise() throws Exception {
        // Initialize the database
        remiseRepository.saveAndFlush(remise);

        // Get the remise
        restRemiseMockMvc.perform(get("/api/remises/{id}", remise.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(remise.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.pourcentage").value(DEFAULT_POURCENTAGE.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingRemise() throws Exception {
        // Get the remise
        restRemiseMockMvc.perform(get("/api/remises/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRemise() throws Exception {
        // Initialize the database
        remiseService.save(remise);

        int databaseSizeBeforeUpdate = remiseRepository.findAll().size();

        // Update the remise
        Remise updatedRemise = remiseRepository.findById(remise.getId()).get();
        // Disconnect from session so that the updates on updatedRemise are not directly saved in db
        em.detach(updatedRemise);
        updatedRemise
            .libelle(UPDATED_LIBELLE)
            .description(UPDATED_DESCRIPTION)
            .pourcentage(UPDATED_POURCENTAGE);

        restRemiseMockMvc.perform(put("/api/remises").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedRemise)))
            .andExpect(status().isOk());

        // Validate the Remise in the database
        List<Remise> remiseList = remiseRepository.findAll();
        assertThat(remiseList).hasSize(databaseSizeBeforeUpdate);
        Remise testRemise = remiseList.get(remiseList.size() - 1);
        assertThat(testRemise.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testRemise.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRemise.getPourcentage()).isEqualTo(UPDATED_POURCENTAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingRemise() throws Exception {
        int databaseSizeBeforeUpdate = remiseRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRemiseMockMvc.perform(put("/api/remises").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remise)))
            .andExpect(status().isBadRequest());

        // Validate the Remise in the database
        List<Remise> remiseList = remiseRepository.findAll();
        assertThat(remiseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRemise() throws Exception {
        // Initialize the database
        remiseService.save(remise);

        int databaseSizeBeforeDelete = remiseRepository.findAll().size();

        // Delete the remise
        restRemiseMockMvc.perform(delete("/api/remises/{id}", remise.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Remise> remiseList = remiseRepository.findAll();
        assertThat(remiseList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
