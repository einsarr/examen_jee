package com.isi.jee.web.rest;

import com.isi.jee.ExamenApp;
import com.isi.jee.domain.Reglement;
import com.isi.jee.repository.ReglementRepository;
import com.isi.jee.service.ReglementService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.isi.jee.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ReglementResource} REST controller.
 */
@SpringBootTest(classes = ExamenApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ReglementResourceIT {

    private static final ZonedDateTime DEFAULT_DATEREGLEMENT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATEREGLEMENT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_MONTANT = 1L;
    private static final Long UPDATED_MONTANT = 2L;

    private static final String DEFAULT_CODEPAIEMENT = "AAAAAAAAAA";
    private static final String UPDATED_CODEPAIEMENT = "BBBBBBBBBB";

    private static final String DEFAULT_MODEREGLEMENT = "AAAAAAAAAA";
    private static final String UPDATED_MODEREGLEMENT = "BBBBBBBBBB";

    @Autowired
    private ReglementRepository reglementRepository;

    @Autowired
    private ReglementService reglementService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restReglementMockMvc;

    private Reglement reglement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Reglement createEntity(EntityManager em) {
        Reglement reglement = new Reglement()
            .datereglement(DEFAULT_DATEREGLEMENT)
            .montant(DEFAULT_MONTANT)
            .codepaiement(DEFAULT_CODEPAIEMENT)
            .modereglement(DEFAULT_MODEREGLEMENT);
        return reglement;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Reglement createUpdatedEntity(EntityManager em) {
        Reglement reglement = new Reglement()
            .datereglement(UPDATED_DATEREGLEMENT)
            .montant(UPDATED_MONTANT)
            .codepaiement(UPDATED_CODEPAIEMENT)
            .modereglement(UPDATED_MODEREGLEMENT);
        return reglement;
    }

    @BeforeEach
    public void initTest() {
        reglement = createEntity(em);
    }

    @Test
    @Transactional
    public void createReglement() throws Exception {
        int databaseSizeBeforeCreate = reglementRepository.findAll().size();
        // Create the Reglement
        restReglementMockMvc.perform(post("/api/reglements").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(reglement)))
            .andExpect(status().isCreated());

        // Validate the Reglement in the database
        List<Reglement> reglementList = reglementRepository.findAll();
        assertThat(reglementList).hasSize(databaseSizeBeforeCreate + 1);
        Reglement testReglement = reglementList.get(reglementList.size() - 1);
        assertThat(testReglement.getDatereglement()).isEqualTo(DEFAULT_DATEREGLEMENT);
        assertThat(testReglement.getMontant()).isEqualTo(DEFAULT_MONTANT);
        assertThat(testReglement.getCodepaiement()).isEqualTo(DEFAULT_CODEPAIEMENT);
        assertThat(testReglement.getModereglement()).isEqualTo(DEFAULT_MODEREGLEMENT);
    }

    @Test
    @Transactional
    public void createReglementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = reglementRepository.findAll().size();

        // Create the Reglement with an existing ID
        reglement.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReglementMockMvc.perform(post("/api/reglements").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(reglement)))
            .andExpect(status().isBadRequest());

        // Validate the Reglement in the database
        List<Reglement> reglementList = reglementRepository.findAll();
        assertThat(reglementList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllReglements() throws Exception {
        // Initialize the database
        reglementRepository.saveAndFlush(reglement);

        // Get all the reglementList
        restReglementMockMvc.perform(get("/api/reglements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(reglement.getId().intValue())))
            .andExpect(jsonPath("$.[*].datereglement").value(hasItem(sameInstant(DEFAULT_DATEREGLEMENT))))
            .andExpect(jsonPath("$.[*].montant").value(hasItem(DEFAULT_MONTANT.intValue())))
            .andExpect(jsonPath("$.[*].codepaiement").value(hasItem(DEFAULT_CODEPAIEMENT)))
            .andExpect(jsonPath("$.[*].modereglement").value(hasItem(DEFAULT_MODEREGLEMENT)));
    }
    
    @Test
    @Transactional
    public void getReglement() throws Exception {
        // Initialize the database
        reglementRepository.saveAndFlush(reglement);

        // Get the reglement
        restReglementMockMvc.perform(get("/api/reglements/{id}", reglement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(reglement.getId().intValue()))
            .andExpect(jsonPath("$.datereglement").value(sameInstant(DEFAULT_DATEREGLEMENT)))
            .andExpect(jsonPath("$.montant").value(DEFAULT_MONTANT.intValue()))
            .andExpect(jsonPath("$.codepaiement").value(DEFAULT_CODEPAIEMENT))
            .andExpect(jsonPath("$.modereglement").value(DEFAULT_MODEREGLEMENT));
    }
    @Test
    @Transactional
    public void getNonExistingReglement() throws Exception {
        // Get the reglement
        restReglementMockMvc.perform(get("/api/reglements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReglement() throws Exception {
        // Initialize the database
        reglementService.save(reglement);

        int databaseSizeBeforeUpdate = reglementRepository.findAll().size();

        // Update the reglement
        Reglement updatedReglement = reglementRepository.findById(reglement.getId()).get();
        // Disconnect from session so that the updates on updatedReglement are not directly saved in db
        em.detach(updatedReglement);
        updatedReglement
            .datereglement(UPDATED_DATEREGLEMENT)
            .montant(UPDATED_MONTANT)
            .codepaiement(UPDATED_CODEPAIEMENT)
            .modereglement(UPDATED_MODEREGLEMENT);

        restReglementMockMvc.perform(put("/api/reglements").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedReglement)))
            .andExpect(status().isOk());

        // Validate the Reglement in the database
        List<Reglement> reglementList = reglementRepository.findAll();
        assertThat(reglementList).hasSize(databaseSizeBeforeUpdate);
        Reglement testReglement = reglementList.get(reglementList.size() - 1);
        assertThat(testReglement.getDatereglement()).isEqualTo(UPDATED_DATEREGLEMENT);
        assertThat(testReglement.getMontant()).isEqualTo(UPDATED_MONTANT);
        assertThat(testReglement.getCodepaiement()).isEqualTo(UPDATED_CODEPAIEMENT);
        assertThat(testReglement.getModereglement()).isEqualTo(UPDATED_MODEREGLEMENT);
    }

    @Test
    @Transactional
    public void updateNonExistingReglement() throws Exception {
        int databaseSizeBeforeUpdate = reglementRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restReglementMockMvc.perform(put("/api/reglements").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(reglement)))
            .andExpect(status().isBadRequest());

        // Validate the Reglement in the database
        List<Reglement> reglementList = reglementRepository.findAll();
        assertThat(reglementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteReglement() throws Exception {
        // Initialize the database
        reglementService.save(reglement);

        int databaseSizeBeforeDelete = reglementRepository.findAll().size();

        // Delete the reglement
        restReglementMockMvc.perform(delete("/api/reglements/{id}", reglement.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Reglement> reglementList = reglementRepository.findAll();
        assertThat(reglementList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
