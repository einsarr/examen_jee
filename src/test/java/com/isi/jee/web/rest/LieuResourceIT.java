package com.isi.jee.web.rest;

import com.isi.jee.ExamenApp;
import com.isi.jee.domain.Lieu;
import com.isi.jee.repository.LieuRepository;
import com.isi.jee.service.LieuService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LieuResource} REST controller.
 */
@SpringBootTest(classes = ExamenApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LieuResourceIT {

    private static final String DEFAULT_NOMLIEU = "AAAAAAAAAA";
    private static final String UPDATED_NOMLIEU = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESSE = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONEFIXE = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONEFIXE = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONEPORTABLE = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONEPORTABLE = "BBBBBBBBBB";

    private static final Integer DEFAULT_PRIX = 1;
    private static final Integer UPDATED_PRIX = 2;

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    @Autowired
    private LieuRepository lieuRepository;

    @Autowired
    private LieuService lieuService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLieuMockMvc;

    private Lieu lieu;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Lieu createEntity(EntityManager em) {
        Lieu lieu = new Lieu()
            .nomlieu(DEFAULT_NOMLIEU)
            .description(DEFAULT_DESCRIPTION)
            .adresse(DEFAULT_ADRESSE)
            .email(DEFAULT_EMAIL)
            .telephonefixe(DEFAULT_TELEPHONEFIXE)
            .telephoneportable(DEFAULT_TELEPHONEPORTABLE)
            .prix(DEFAULT_PRIX)
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE);
        return lieu;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Lieu createUpdatedEntity(EntityManager em) {
        Lieu lieu = new Lieu()
            .nomlieu(UPDATED_NOMLIEU)
            .description(UPDATED_DESCRIPTION)
            .adresse(UPDATED_ADRESSE)
            .email(UPDATED_EMAIL)
            .telephonefixe(UPDATED_TELEPHONEFIXE)
            .telephoneportable(UPDATED_TELEPHONEPORTABLE)
            .prix(UPDATED_PRIX)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE);
        return lieu;
    }

    @BeforeEach
    public void initTest() {
        lieu = createEntity(em);
    }

    @Test
    @Transactional
    public void createLieu() throws Exception {
        int databaseSizeBeforeCreate = lieuRepository.findAll().size();
        // Create the Lieu
        restLieuMockMvc.perform(post("/api/lieus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lieu)))
            .andExpect(status().isCreated());

        // Validate the Lieu in the database
        List<Lieu> lieuList = lieuRepository.findAll();
        assertThat(lieuList).hasSize(databaseSizeBeforeCreate + 1);
        Lieu testLieu = lieuList.get(lieuList.size() - 1);
        assertThat(testLieu.getNomlieu()).isEqualTo(DEFAULT_NOMLIEU);
        assertThat(testLieu.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testLieu.getAdresse()).isEqualTo(DEFAULT_ADRESSE);
        assertThat(testLieu.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testLieu.getTelephonefixe()).isEqualTo(DEFAULT_TELEPHONEFIXE);
        assertThat(testLieu.getTelephoneportable()).isEqualTo(DEFAULT_TELEPHONEPORTABLE);
        assertThat(testLieu.getPrix()).isEqualTo(DEFAULT_PRIX);
        assertThat(testLieu.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testLieu.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createLieuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = lieuRepository.findAll().size();

        // Create the Lieu with an existing ID
        lieu.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLieuMockMvc.perform(post("/api/lieus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lieu)))
            .andExpect(status().isBadRequest());

        // Validate the Lieu in the database
        List<Lieu> lieuList = lieuRepository.findAll();
        assertThat(lieuList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNomlieuIsRequired() throws Exception {
        int databaseSizeBeforeTest = lieuRepository.findAll().size();
        // set the field null
        lieu.setNomlieu(null);

        // Create the Lieu, which fails.


        restLieuMockMvc.perform(post("/api/lieus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lieu)))
            .andExpect(status().isBadRequest());

        List<Lieu> lieuList = lieuRepository.findAll();
        assertThat(lieuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLieus() throws Exception {
        // Initialize the database
        lieuRepository.saveAndFlush(lieu);

        // Get all the lieuList
        restLieuMockMvc.perform(get("/api/lieus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lieu.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomlieu").value(hasItem(DEFAULT_NOMLIEU)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].adresse").value(hasItem(DEFAULT_ADRESSE)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].telephonefixe").value(hasItem(DEFAULT_TELEPHONEFIXE)))
            .andExpect(jsonPath("$.[*].telephoneportable").value(hasItem(DEFAULT_TELEPHONEPORTABLE)))
            .andExpect(jsonPath("$.[*].prix").value(hasItem(DEFAULT_PRIX)))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))));
    }
    
    @Test
    @Transactional
    public void getLieu() throws Exception {
        // Initialize the database
        lieuRepository.saveAndFlush(lieu);

        // Get the lieu
        restLieuMockMvc.perform(get("/api/lieus/{id}", lieu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(lieu.getId().intValue()))
            .andExpect(jsonPath("$.nomlieu").value(DEFAULT_NOMLIEU))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.adresse").value(DEFAULT_ADRESSE))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.telephonefixe").value(DEFAULT_TELEPHONEFIXE))
            .andExpect(jsonPath("$.telephoneportable").value(DEFAULT_TELEPHONEPORTABLE))
            .andExpect(jsonPath("$.prix").value(DEFAULT_PRIX))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64Utils.encodeToString(DEFAULT_IMAGE)));
    }
    @Test
    @Transactional
    public void getNonExistingLieu() throws Exception {
        // Get the lieu
        restLieuMockMvc.perform(get("/api/lieus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLieu() throws Exception {
        // Initialize the database
        lieuService.save(lieu);

        int databaseSizeBeforeUpdate = lieuRepository.findAll().size();

        // Update the lieu
        Lieu updatedLieu = lieuRepository.findById(lieu.getId()).get();
        // Disconnect from session so that the updates on updatedLieu are not directly saved in db
        em.detach(updatedLieu);
        updatedLieu
            .nomlieu(UPDATED_NOMLIEU)
            .description(UPDATED_DESCRIPTION)
            .adresse(UPDATED_ADRESSE)
            .email(UPDATED_EMAIL)
            .telephonefixe(UPDATED_TELEPHONEFIXE)
            .telephoneportable(UPDATED_TELEPHONEPORTABLE)
            .prix(UPDATED_PRIX)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE);

        restLieuMockMvc.perform(put("/api/lieus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLieu)))
            .andExpect(status().isOk());

        // Validate the Lieu in the database
        List<Lieu> lieuList = lieuRepository.findAll();
        assertThat(lieuList).hasSize(databaseSizeBeforeUpdate);
        Lieu testLieu = lieuList.get(lieuList.size() - 1);
        assertThat(testLieu.getNomlieu()).isEqualTo(UPDATED_NOMLIEU);
        assertThat(testLieu.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testLieu.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testLieu.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testLieu.getTelephonefixe()).isEqualTo(UPDATED_TELEPHONEFIXE);
        assertThat(testLieu.getTelephoneportable()).isEqualTo(UPDATED_TELEPHONEPORTABLE);
        assertThat(testLieu.getPrix()).isEqualTo(UPDATED_PRIX);
        assertThat(testLieu.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testLieu.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingLieu() throws Exception {
        int databaseSizeBeforeUpdate = lieuRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLieuMockMvc.perform(put("/api/lieus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lieu)))
            .andExpect(status().isBadRequest());

        // Validate the Lieu in the database
        List<Lieu> lieuList = lieuRepository.findAll();
        assertThat(lieuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLieu() throws Exception {
        // Initialize the database
        lieuService.save(lieu);

        int databaseSizeBeforeDelete = lieuRepository.findAll().size();

        // Delete the lieu
        restLieuMockMvc.perform(delete("/api/lieus/{id}", lieu.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Lieu> lieuList = lieuRepository.findAll();
        assertThat(lieuList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
