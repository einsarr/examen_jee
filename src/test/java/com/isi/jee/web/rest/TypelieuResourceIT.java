package com.isi.jee.web.rest;

import com.isi.jee.ExamenApp;
import com.isi.jee.domain.Typelieu;
import com.isi.jee.repository.TypelieuRepository;
import com.isi.jee.service.TypelieuService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TypelieuResource} REST controller.
 */
@SpringBootTest(classes = ExamenApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class TypelieuResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    @Autowired
    private TypelieuRepository typelieuRepository;

    @Autowired
    private TypelieuService typelieuService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTypelieuMockMvc;

    private Typelieu typelieu;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Typelieu createEntity(EntityManager em) {
        Typelieu typelieu = new Typelieu()
            .libelle(DEFAULT_LIBELLE);
        return typelieu;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Typelieu createUpdatedEntity(EntityManager em) {
        Typelieu typelieu = new Typelieu()
            .libelle(UPDATED_LIBELLE);
        return typelieu;
    }

    @BeforeEach
    public void initTest() {
        typelieu = createEntity(em);
    }

    @Test
    @Transactional
    public void createTypelieu() throws Exception {
        int databaseSizeBeforeCreate = typelieuRepository.findAll().size();
        // Create the Typelieu
        restTypelieuMockMvc.perform(post("/api/typelieus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typelieu)))
            .andExpect(status().isCreated());

        // Validate the Typelieu in the database
        List<Typelieu> typelieuList = typelieuRepository.findAll();
        assertThat(typelieuList).hasSize(databaseSizeBeforeCreate + 1);
        Typelieu testTypelieu = typelieuList.get(typelieuList.size() - 1);
        assertThat(testTypelieu.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
    }

    @Test
    @Transactional
    public void createTypelieuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = typelieuRepository.findAll().size();

        // Create the Typelieu with an existing ID
        typelieu.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTypelieuMockMvc.perform(post("/api/typelieus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typelieu)))
            .andExpect(status().isBadRequest());

        // Validate the Typelieu in the database
        List<Typelieu> typelieuList = typelieuRepository.findAll();
        assertThat(typelieuList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = typelieuRepository.findAll().size();
        // set the field null
        typelieu.setLibelle(null);

        // Create the Typelieu, which fails.


        restTypelieuMockMvc.perform(post("/api/typelieus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typelieu)))
            .andExpect(status().isBadRequest());

        List<Typelieu> typelieuList = typelieuRepository.findAll();
        assertThat(typelieuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTypelieus() throws Exception {
        // Initialize the database
        typelieuRepository.saveAndFlush(typelieu);

        // Get all the typelieuList
        restTypelieuMockMvc.perform(get("/api/typelieus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(typelieu.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)));
    }
    
    @Test
    @Transactional
    public void getTypelieu() throws Exception {
        // Initialize the database
        typelieuRepository.saveAndFlush(typelieu);

        // Get the typelieu
        restTypelieuMockMvc.perform(get("/api/typelieus/{id}", typelieu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(typelieu.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE));
    }
    @Test
    @Transactional
    public void getNonExistingTypelieu() throws Exception {
        // Get the typelieu
        restTypelieuMockMvc.perform(get("/api/typelieus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTypelieu() throws Exception {
        // Initialize the database
        typelieuService.save(typelieu);

        int databaseSizeBeforeUpdate = typelieuRepository.findAll().size();

        // Update the typelieu
        Typelieu updatedTypelieu = typelieuRepository.findById(typelieu.getId()).get();
        // Disconnect from session so that the updates on updatedTypelieu are not directly saved in db
        em.detach(updatedTypelieu);
        updatedTypelieu
            .libelle(UPDATED_LIBELLE);

        restTypelieuMockMvc.perform(put("/api/typelieus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedTypelieu)))
            .andExpect(status().isOk());

        // Validate the Typelieu in the database
        List<Typelieu> typelieuList = typelieuRepository.findAll();
        assertThat(typelieuList).hasSize(databaseSizeBeforeUpdate);
        Typelieu testTypelieu = typelieuList.get(typelieuList.size() - 1);
        assertThat(testTypelieu.getLibelle()).isEqualTo(UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    public void updateNonExistingTypelieu() throws Exception {
        int databaseSizeBeforeUpdate = typelieuRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTypelieuMockMvc.perform(put("/api/typelieus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typelieu)))
            .andExpect(status().isBadRequest());

        // Validate the Typelieu in the database
        List<Typelieu> typelieuList = typelieuRepository.findAll();
        assertThat(typelieuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTypelieu() throws Exception {
        // Initialize the database
        typelieuService.save(typelieu);

        int databaseSizeBeforeDelete = typelieuRepository.findAll().size();

        // Delete the typelieu
        restTypelieuMockMvc.perform(delete("/api/typelieus/{id}", typelieu.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Typelieu> typelieuList = typelieuRepository.findAll();
        assertThat(typelieuList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
