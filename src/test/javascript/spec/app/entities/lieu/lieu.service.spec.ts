import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { LieuService } from 'app/entities/lieu/lieu.service';
import { ILieu, Lieu } from 'app/shared/model/lieu.model';

describe('Service Tests', () => {
  describe('Lieu Service', () => {
    let injector: TestBed;
    let service: LieuService;
    let httpMock: HttpTestingController;
    let elemDefault: ILieu;
    let expectedResult: ILieu | ILieu[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(LieuService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Lieu(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 0, 'image/png', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Lieu', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Lieu()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Lieu', () => {
        const returnedFromService = Object.assign(
          {
            nomlieu: 'BBBBBB',
            description: 'BBBBBB',
            adresse: 'BBBBBB',
            email: 'BBBBBB',
            telephonefixe: 'BBBBBB',
            telephoneportable: 'BBBBBB',
            prix: 1,
            image: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Lieu', () => {
        const returnedFromService = Object.assign(
          {
            nomlieu: 'BBBBBB',
            description: 'BBBBBB',
            adresse: 'BBBBBB',
            email: 'BBBBBB',
            telephonefixe: 'BBBBBB',
            telephoneportable: 'BBBBBB',
            prix: 1,
            image: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Lieu', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
