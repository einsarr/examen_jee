import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ExamenTestModule } from '../../../test.module';
import { TypelieuUpdateComponent } from 'app/entities/typelieu/typelieu-update.component';
import { TypelieuService } from 'app/entities/typelieu/typelieu.service';
import { Typelieu } from 'app/shared/model/typelieu.model';

describe('Component Tests', () => {
  describe('Typelieu Management Update Component', () => {
    let comp: TypelieuUpdateComponent;
    let fixture: ComponentFixture<TypelieuUpdateComponent>;
    let service: TypelieuService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ExamenTestModule],
        declarations: [TypelieuUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(TypelieuUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TypelieuUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TypelieuService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Typelieu(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Typelieu();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
