import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ExamenTestModule } from '../../../test.module';
import { TypelieuDetailComponent } from 'app/entities/typelieu/typelieu-detail.component';
import { Typelieu } from 'app/shared/model/typelieu.model';

describe('Component Tests', () => {
  describe('Typelieu Management Detail Component', () => {
    let comp: TypelieuDetailComponent;
    let fixture: ComponentFixture<TypelieuDetailComponent>;
    const route = ({ data: of({ typelieu: new Typelieu(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ExamenTestModule],
        declarations: [TypelieuDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TypelieuDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TypelieuDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load typelieu on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.typelieu).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
