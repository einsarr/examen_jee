import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ExamenTestModule } from '../../../test.module';
import { FeteUpdateComponent } from 'app/entities/fete/fete-update.component';
import { FeteService } from 'app/entities/fete/fete.service';
import { Fete } from 'app/shared/model/fete.model';

describe('Component Tests', () => {
  describe('Fete Management Update Component', () => {
    let comp: FeteUpdateComponent;
    let fixture: ComponentFixture<FeteUpdateComponent>;
    let service: FeteService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ExamenTestModule],
        declarations: [FeteUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FeteUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FeteUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FeteService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Fete(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Fete();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
