import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ExamenTestModule } from '../../../test.module';
import { FeteDetailComponent } from 'app/entities/fete/fete-detail.component';
import { Fete } from 'app/shared/model/fete.model';

describe('Component Tests', () => {
  describe('Fete Management Detail Component', () => {
    let comp: FeteDetailComponent;
    let fixture: ComponentFixture<FeteDetailComponent>;
    const route = ({ data: of({ fete: new Fete(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ExamenTestModule],
        declarations: [FeteDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FeteDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FeteDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fete on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fete).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
