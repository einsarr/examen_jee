export interface IRemise {
  id?: number;
  libelle?: string;
  description?: string;
  pourcentage?: number;
}

export class Remise implements IRemise {
  constructor(public id?: number, public libelle?: string, public description?: string, public pourcentage?: number) {}
}
