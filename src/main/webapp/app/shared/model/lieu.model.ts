import { ITypelieu } from 'app/shared/model/typelieu.model';

export interface ILieu {
  id?: number;
  nomlieu?: string;
  description?: string;
  adresse?: string;
  email?: string;
  telephonefixe?: string;
  telephoneportable?: string;
  prix?: number;
  imageContentType?: string;
  image?: any;
  typelieu?: ITypelieu;
}

export class Lieu implements ILieu {
  constructor(
    public id?: number,
    public nomlieu?: string,
    public description?: string,
    public adresse?: string,
    public email?: string,
    public telephonefixe?: string,
    public telephoneportable?: string,
    public prix?: number,
    public imageContentType?: string,
    public image?: any,
    public typelieu?: ITypelieu
  ) {}
}
