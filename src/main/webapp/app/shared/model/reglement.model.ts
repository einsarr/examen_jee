import { Moment } from 'moment';
import { ICommande } from 'app/shared/model/commande.model';

export interface IReglement {
  id?: number;
  datereglement?: Moment;
  montant?: number;
  codepaiement?: string;
  modereglement?: string;
  commande?: ICommande;
}

export class Reglement implements IReglement {
  constructor(
    public id?: number,
    public datereglement?: Moment,
    public montant?: number,
    public codepaiement?: string,
    public modereglement?: string,
    public commande?: ICommande
  ) {}
}
