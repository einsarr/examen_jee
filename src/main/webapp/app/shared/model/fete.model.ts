import { IRemise } from 'app/shared/model/remise.model';

export interface IFete {
  id?: number;
  libelle?: string;
  description?: string;
  etat?: string;
  remise?: IRemise;
}

export class Fete implements IFete {
  constructor(public id?: number, public libelle?: string, public description?: string, public etat?: string, public remise?: IRemise) {}
}
