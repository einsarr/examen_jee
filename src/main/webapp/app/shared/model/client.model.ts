import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';

export interface IClient {
  id?: number;
  prenom?: string;
  nom?: string;
  datenaissance?: Moment;
  lieunaissance?: string;
  cni?: string;
  telephone?: string;
  email?: string;
  sexe?: string;
  adresse?: string;
  user?: IUser;
}

export class Client implements IClient {
  constructor(
    public id?: number,
    public prenom?: string,
    public nom?: string,
    public datenaissance?: Moment,
    public lieunaissance?: string,
    public cni?: string,
    public telephone?: string,
    public email?: string,
    public sexe?: string,
    public adresse?: string,
    public user?: IUser
  ) {}
}
