import { Moment } from 'moment';
import { IClient } from 'app/shared/model/client.model';
import { ILieu } from 'app/shared/model/lieu.model';
import { IEvenement } from 'app/shared/model/evenement.model';
import { IFete } from 'app/shared/model/fete.model';

export interface ICommande {
  id?: number;
  datedebut?: Moment;
  datefin?: Moment;
  heuredebut?: number;
  heurefin?: number;
  pointsfidelite?: number;
  client?: IClient;
  lieu?: ILieu;
  evenement?: IEvenement;
  fete?: IFete;
}

export class Commande implements ICommande {
  constructor(
    public id?: number,
    public datedebut?: Moment,
    public datefin?: Moment,
    public heuredebut?: number,
    public heurefin?: number,
    public pointsfidelite?: number,
    public client?: IClient,
    public lieu?: ILieu,
    public evenement?: IEvenement,
    public fete?: IFete
  ) {}
}
