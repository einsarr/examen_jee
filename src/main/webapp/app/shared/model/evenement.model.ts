import { IRemise } from 'app/shared/model/remise.model';

export interface IEvenement {
  id?: number;
  libelle?: string;
  etat?: string;
  remise?: IRemise;
}

export class Evenement implements IEvenement {
  constructor(public id?: number, public libelle?: string, public etat?: string, public remise?: IRemise) {}
}
