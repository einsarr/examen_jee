export interface ITypelieu {
  id?: number;
  libelle?: string;
}

export class Typelieu implements ITypelieu {
  constructor(public id?: number, public libelle?: string) {}
}
