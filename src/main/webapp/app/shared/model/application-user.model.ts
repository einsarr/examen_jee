export interface IApplicationUser {
  id?: number;
  telephone?: string;
  adresse?: string;
}

export class ApplicationUser implements IApplicationUser {
  constructor(public id?: number, public telephone?: string, public adresse?: string) {}
}
