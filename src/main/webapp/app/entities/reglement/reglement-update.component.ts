import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IReglement, Reglement } from 'app/shared/model/reglement.model';
import { ReglementService } from './reglement.service';
import { ICommande } from 'app/shared/model/commande.model';
import { CommandeService } from 'app/entities/commande/commande.service';

@Component({
  selector: 'jhi-reglement-update',
  templateUrl: './reglement-update.component.html',
})
export class ReglementUpdateComponent implements OnInit {
  isSaving = false;
  commandes: ICommande[] = [];

  editForm = this.fb.group({
    id: [],
    datereglement: [],
    montant: [],
    codepaiement: [],
    modereglement: [],
    commande: [],
  });

  constructor(
    protected reglementService: ReglementService,
    protected commandeService: CommandeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ reglement }) => {
      if (!reglement.id) {
        const today = moment().startOf('day');
        reglement.datereglement = today;
      }

      this.updateForm(reglement);

      this.commandeService.query().subscribe((res: HttpResponse<ICommande[]>) => (this.commandes = res.body || []));
    });
  }

  updateForm(reglement: IReglement): void {
    this.editForm.patchValue({
      id: reglement.id,
      datereglement: reglement.datereglement ? reglement.datereglement.format(DATE_TIME_FORMAT) : null,
      montant: reglement.montant,
      codepaiement: reglement.codepaiement,
      modereglement: reglement.modereglement,
      commande: reglement.commande,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const reglement = this.createFromForm();
    if (reglement.id !== undefined) {
      this.subscribeToSaveResponse(this.reglementService.update(reglement));
    } else {
      this.subscribeToSaveResponse(this.reglementService.create(reglement));
    }
  }

  private createFromForm(): IReglement {
    return {
      ...new Reglement(),
      id: this.editForm.get(['id'])!.value,
      datereglement: this.editForm.get(['datereglement'])!.value
        ? moment(this.editForm.get(['datereglement'])!.value, DATE_TIME_FORMAT)
        : undefined,
      montant: this.editForm.get(['montant'])!.value,
      codepaiement: this.editForm.get(['codepaiement'])!.value,
      modereglement: this.editForm.get(['modereglement'])!.value,
      commande: this.editForm.get(['commande'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IReglement>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICommande): any {
    return item.id;
  }
}
