import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'typelieu',
        loadChildren: () => import('./typelieu/typelieu.module').then(m => m.ExamenTypelieuModule),
      },
      {
        path: 'lieu',
        loadChildren: () => import('./lieu/lieu.module').then(m => m.ExamenLieuModule),
      },
      {
        path: 'evenement',
        loadChildren: () => import('./evenement/evenement.module').then(m => m.ExamenEvenementModule),
      },
      {
        path: 'fete',
        loadChildren: () => import('./fete/fete.module').then(m => m.ExamenFeteModule),
      },
      {
        path: 'remise',
        loadChildren: () => import('./remise/remise.module').then(m => m.ExamenRemiseModule),
      },
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.ExamenClientModule),
      },
      {
        path: 'commande',
        loadChildren: () => import('./commande/commande.module').then(m => m.ExamenCommandeModule),
      },
      {
        path: 'reglement',
        loadChildren: () => import('./reglement/reglement.module').then(m => m.ExamenReglementModule),
      },
      {
        path: 'application-user',
        loadChildren: () => import('./application-user/application-user.module').then(m => m.ExamenApplicationUserModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class ExamenEntityModule {}
