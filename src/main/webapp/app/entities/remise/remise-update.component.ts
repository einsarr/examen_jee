import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRemise, Remise } from 'app/shared/model/remise.model';
import { RemiseService } from './remise.service';

@Component({
  selector: 'jhi-remise-update',
  templateUrl: './remise-update.component.html',
})
export class RemiseUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    libelle: [],
    description: [],
    pourcentage: [],
  });

  constructor(protected remiseService: RemiseService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ remise }) => {
      this.updateForm(remise);
    });
  }

  updateForm(remise: IRemise): void {
    this.editForm.patchValue({
      id: remise.id,
      libelle: remise.libelle,
      description: remise.description,
      pourcentage: remise.pourcentage,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const remise = this.createFromForm();
    if (remise.id !== undefined) {
      this.subscribeToSaveResponse(this.remiseService.update(remise));
    } else {
      this.subscribeToSaveResponse(this.remiseService.create(remise));
    }
  }

  private createFromForm(): IRemise {
    return {
      ...new Remise(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
      description: this.editForm.get(['description'])!.value,
      pourcentage: this.editForm.get(['pourcentage'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRemise>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
