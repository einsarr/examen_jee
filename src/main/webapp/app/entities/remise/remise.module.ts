import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ExamenSharedModule } from 'app/shared/shared.module';
import { RemiseComponent } from './remise.component';
import { RemiseDetailComponent } from './remise-detail.component';
import { RemiseUpdateComponent } from './remise-update.component';
import { RemiseDeleteDialogComponent } from './remise-delete-dialog.component';
import { remiseRoute } from './remise.route';

@NgModule({
  imports: [ExamenSharedModule, RouterModule.forChild(remiseRoute)],
  declarations: [RemiseComponent, RemiseDetailComponent, RemiseUpdateComponent, RemiseDeleteDialogComponent],
  entryComponents: [RemiseDeleteDialogComponent],
})
export class ExamenRemiseModule {}
