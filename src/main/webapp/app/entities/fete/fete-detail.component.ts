import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFete } from 'app/shared/model/fete.model';

@Component({
  selector: 'jhi-fete-detail',
  templateUrl: './fete-detail.component.html',
})
export class FeteDetailComponent implements OnInit {
  fete: IFete | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fete }) => (this.fete = fete));
  }

  previousState(): void {
    window.history.back();
  }
}
