import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFete, Fete } from 'app/shared/model/fete.model';
import { FeteService } from './fete.service';
import { IRemise } from 'app/shared/model/remise.model';
import { RemiseService } from 'app/entities/remise/remise.service';

@Component({
  selector: 'jhi-fete-update',
  templateUrl: './fete-update.component.html',
})
export class FeteUpdateComponent implements OnInit {
  isSaving = false;
  remises: IRemise[] = [];

  editForm = this.fb.group({
    id: [],
    libelle: [],
    description: [],
    etat: [],
    remise: [],
  });

  constructor(
    protected feteService: FeteService,
    protected remiseService: RemiseService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fete }) => {
      this.updateForm(fete);

      this.remiseService.query().subscribe((res: HttpResponse<IRemise[]>) => (this.remises = res.body || []));
    });
  }

  updateForm(fete: IFete): void {
    this.editForm.patchValue({
      id: fete.id,
      libelle: fete.libelle,
      description: fete.description,
      etat: fete.etat,
      remise: fete.remise,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fete = this.createFromForm();
    if (fete.id !== undefined) {
      this.subscribeToSaveResponse(this.feteService.update(fete));
    } else {
      this.subscribeToSaveResponse(this.feteService.create(fete));
    }
  }

  private createFromForm(): IFete {
    return {
      ...new Fete(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
      description: this.editForm.get(['description'])!.value,
      etat: this.editForm.get(['etat'])!.value,
      remise: this.editForm.get(['remise'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFete>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IRemise): any {
    return item.id;
  }
}
