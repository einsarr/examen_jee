import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFete, Fete } from 'app/shared/model/fete.model';
import { FeteService } from './fete.service';
import { FeteComponent } from './fete.component';
import { FeteDetailComponent } from './fete-detail.component';
import { FeteUpdateComponent } from './fete-update.component';

@Injectable({ providedIn: 'root' })
export class FeteResolve implements Resolve<IFete> {
  constructor(private service: FeteService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFete> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fete: HttpResponse<Fete>) => {
          if (fete.body) {
            return of(fete.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Fete());
  }
}

export const feteRoute: Routes = [
  {
    path: '',
    component: FeteComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'examenApp.fete.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FeteDetailComponent,
    resolve: {
      fete: FeteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'examenApp.fete.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FeteUpdateComponent,
    resolve: {
      fete: FeteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'examenApp.fete.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FeteUpdateComponent,
    resolve: {
      fete: FeteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'examenApp.fete.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
