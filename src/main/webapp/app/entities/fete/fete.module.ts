import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ExamenSharedModule } from 'app/shared/shared.module';
import { FeteComponent } from './fete.component';
import { FeteDetailComponent } from './fete-detail.component';
import { FeteUpdateComponent } from './fete-update.component';
import { FeteDeleteDialogComponent } from './fete-delete-dialog.component';
import { feteRoute } from './fete.route';

@NgModule({
  imports: [ExamenSharedModule, RouterModule.forChild(feteRoute)],
  declarations: [FeteComponent, FeteDetailComponent, FeteUpdateComponent, FeteDeleteDialogComponent],
  entryComponents: [FeteDeleteDialogComponent],
})
export class ExamenFeteModule {}
