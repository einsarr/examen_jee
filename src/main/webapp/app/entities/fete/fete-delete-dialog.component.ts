import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFete } from 'app/shared/model/fete.model';
import { FeteService } from './fete.service';

@Component({
  templateUrl: './fete-delete-dialog.component.html',
})
export class FeteDeleteDialogComponent {
  fete?: IFete;

  constructor(protected feteService: FeteService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.feteService.delete(id).subscribe(() => {
      this.eventManager.broadcast('feteListModification');
      this.activeModal.close();
    });
  }
}
