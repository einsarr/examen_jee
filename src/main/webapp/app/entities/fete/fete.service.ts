import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFete } from 'app/shared/model/fete.model';

type EntityResponseType = HttpResponse<IFete>;
type EntityArrayResponseType = HttpResponse<IFete[]>;

@Injectable({ providedIn: 'root' })
export class FeteService {
  public resourceUrl = SERVER_API_URL + 'api/fetes';

  constructor(protected http: HttpClient) {}

  create(fete: IFete): Observable<EntityResponseType> {
    return this.http.post<IFete>(this.resourceUrl, fete, { observe: 'response' });
  }

  update(fete: IFete): Observable<EntityResponseType> {
    return this.http.put<IFete>(this.resourceUrl, fete, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFete>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFete[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
