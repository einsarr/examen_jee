import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { ILieu, Lieu } from 'app/shared/model/lieu.model';
import { LieuService } from './lieu.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { ITypelieu } from 'app/shared/model/typelieu.model';
import { TypelieuService } from 'app/entities/typelieu/typelieu.service';

@Component({
  selector: 'jhi-lieu-update',
  templateUrl: './lieu-update.component.html',
})
export class LieuUpdateComponent implements OnInit {
  isSaving = false;
  typelieus: ITypelieu[] = [];

  editForm = this.fb.group({
    id: [],
    nomlieu: [null, [Validators.required]],
    description: [],
    adresse: [],
    email: [],
    telephonefixe: [],
    telephoneportable: [],
    prix: [],
    image: [],
    imageContentType: [],
    typelieu: [],
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected lieuService: LieuService,
    protected typelieuService: TypelieuService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ lieu }) => {
      this.updateForm(lieu);

      this.typelieuService.query().subscribe((res: HttpResponse<ITypelieu[]>) => (this.typelieus = res.body || []));
    });
  }

  updateForm(lieu: ILieu): void {
    this.editForm.patchValue({
      id: lieu.id,
      nomlieu: lieu.nomlieu,
      description: lieu.description,
      adresse: lieu.adresse,
      email: lieu.email,
      telephonefixe: lieu.telephonefixe,
      telephoneportable: lieu.telephoneportable,
      prix: lieu.prix,
      image: lieu.image,
      imageContentType: lieu.imageContentType,
      typelieu: lieu.typelieu,
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: any, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('examenApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const lieu = this.createFromForm();
    if (lieu.id !== undefined) {
      this.subscribeToSaveResponse(this.lieuService.update(lieu));
    } else {
      this.subscribeToSaveResponse(this.lieuService.create(lieu));
    }
  }

  private createFromForm(): ILieu {
    return {
      ...new Lieu(),
      id: this.editForm.get(['id'])!.value,
      nomlieu: this.editForm.get(['nomlieu'])!.value,
      description: this.editForm.get(['description'])!.value,
      adresse: this.editForm.get(['adresse'])!.value,
      email: this.editForm.get(['email'])!.value,
      telephonefixe: this.editForm.get(['telephonefixe'])!.value,
      telephoneportable: this.editForm.get(['telephoneportable'])!.value,
      prix: this.editForm.get(['prix'])!.value,
      imageContentType: this.editForm.get(['imageContentType'])!.value,
      image: this.editForm.get(['image'])!.value,
      typelieu: this.editForm.get(['typelieu'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILieu>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ITypelieu): any {
    return item.id;
  }
}
