import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICommande, Commande } from 'app/shared/model/commande.model';
import { CommandeService } from './commande.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client/client.service';
import { ILieu } from 'app/shared/model/lieu.model';
import { LieuService } from 'app/entities/lieu/lieu.service';
import { IEvenement } from 'app/shared/model/evenement.model';
import { EvenementService } from 'app/entities/evenement/evenement.service';
import { IFete } from 'app/shared/model/fete.model';
import { FeteService } from 'app/entities/fete/fete.service';

type SelectableEntity = IClient | ILieu | IEvenement | IFete;

@Component({
  selector: 'jhi-commande-update',
  templateUrl: './commande-update.component.html',
})
export class CommandeUpdateComponent implements OnInit {
  isSaving = false;
  clients: IClient[] = [];
  lieus: ILieu[] = [];
  evenements: IEvenement[] = [];
  fetes: IFete[] = [];
  datedebutDp: any;
  datefinDp: any;

  editForm = this.fb.group({
    id: [],
    datedebut: [],
    datefin: [],
    heuredebut: [],
    heurefin: [],
    pointsfidelite: [],
    client: [],
    lieu: [],
    evenement: [],
    fete: [],
  });

  constructor(
    protected commandeService: CommandeService,
    protected clientService: ClientService,
    protected lieuService: LieuService,
    protected evenementService: EvenementService,
    protected feteService: FeteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commande }) => {
      this.updateForm(commande);

      this.clientService.query().subscribe((res: HttpResponse<IClient[]>) => (this.clients = res.body || []));

      this.lieuService.query().subscribe((res: HttpResponse<ILieu[]>) => (this.lieus = res.body || []));

      this.evenementService.query().subscribe((res: HttpResponse<IEvenement[]>) => (this.evenements = res.body || []));

      this.feteService.query().subscribe((res: HttpResponse<IFete[]>) => (this.fetes = res.body || []));
    });
  }

  updateForm(commande: ICommande): void {
    this.editForm.patchValue({
      id: commande.id,
      datedebut: commande.datedebut,
      datefin: commande.datefin,
      heuredebut: commande.heuredebut,
      heurefin: commande.heurefin,
      pointsfidelite: commande.pointsfidelite,
      client: commande.client,
      lieu: commande.lieu,
      evenement: commande.evenement,
      fete: commande.fete,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const commande = this.createFromForm();
    if (commande.id !== undefined) {
      this.subscribeToSaveResponse(this.commandeService.update(commande));
    } else {
      this.subscribeToSaveResponse(this.commandeService.create(commande));
    }
  }

  private createFromForm(): ICommande {
    return {
      ...new Commande(),
      id: this.editForm.get(['id'])!.value,
      datedebut: this.editForm.get(['datedebut'])!.value,
      datefin: this.editForm.get(['datefin'])!.value,
      heuredebut: this.editForm.get(['heuredebut'])!.value,
      heurefin: this.editForm.get(['heurefin'])!.value,
      pointsfidelite: this.editForm.get(['pointsfidelite'])!.value,
      client: this.editForm.get(['client'])!.value,
      lieu: this.editForm.get(['lieu'])!.value,
      evenement: this.editForm.get(['evenement'])!.value,
      fete: this.editForm.get(['fete'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommande>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
