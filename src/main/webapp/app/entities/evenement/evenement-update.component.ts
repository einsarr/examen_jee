import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IEvenement, Evenement } from 'app/shared/model/evenement.model';
import { EvenementService } from './evenement.service';
import { IRemise } from 'app/shared/model/remise.model';
import { RemiseService } from 'app/entities/remise/remise.service';

@Component({
  selector: 'jhi-evenement-update',
  templateUrl: './evenement-update.component.html',
})
export class EvenementUpdateComponent implements OnInit {
  isSaving = false;
  remises: IRemise[] = [];

  editForm = this.fb.group({
    id: [],
    libelle: [],
    etat: [],
    remise: [],
  });

  constructor(
    protected evenementService: EvenementService,
    protected remiseService: RemiseService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ evenement }) => {
      this.updateForm(evenement);

      this.remiseService.query().subscribe((res: HttpResponse<IRemise[]>) => (this.remises = res.body || []));
    });
  }

  updateForm(evenement: IEvenement): void {
    this.editForm.patchValue({
      id: evenement.id,
      libelle: evenement.libelle,
      etat: evenement.etat,
      remise: evenement.remise,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const evenement = this.createFromForm();
    if (evenement.id !== undefined) {
      this.subscribeToSaveResponse(this.evenementService.update(evenement));
    } else {
      this.subscribeToSaveResponse(this.evenementService.create(evenement));
    }
  }

  private createFromForm(): IEvenement {
    return {
      ...new Evenement(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
      etat: this.editForm.get(['etat'])!.value,
      remise: this.editForm.get(['remise'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEvenement>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IRemise): any {
    return item.id;
  }
}
