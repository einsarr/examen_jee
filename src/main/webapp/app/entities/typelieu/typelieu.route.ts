import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITypelieu, Typelieu } from 'app/shared/model/typelieu.model';
import { TypelieuService } from './typelieu.service';
import { TypelieuComponent } from './typelieu.component';
import { TypelieuDetailComponent } from './typelieu-detail.component';
import { TypelieuUpdateComponent } from './typelieu-update.component';

@Injectable({ providedIn: 'root' })
export class TypelieuResolve implements Resolve<ITypelieu> {
  constructor(private service: TypelieuService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITypelieu> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((typelieu: HttpResponse<Typelieu>) => {
          if (typelieu.body) {
            return of(typelieu.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Typelieu());
  }
}

export const typelieuRoute: Routes = [
  {
    path: '',
    component: TypelieuComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'examenApp.typelieu.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TypelieuDetailComponent,
    resolve: {
      typelieu: TypelieuResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'examenApp.typelieu.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TypelieuUpdateComponent,
    resolve: {
      typelieu: TypelieuResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'examenApp.typelieu.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TypelieuUpdateComponent,
    resolve: {
      typelieu: TypelieuResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'examenApp.typelieu.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
