import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITypelieu } from 'app/shared/model/typelieu.model';

type EntityResponseType = HttpResponse<ITypelieu>;
type EntityArrayResponseType = HttpResponse<ITypelieu[]>;

@Injectable({ providedIn: 'root' })
export class TypelieuService {
  public resourceUrl = SERVER_API_URL + 'api/typelieus';

  constructor(protected http: HttpClient) {}

  create(typelieu: ITypelieu): Observable<EntityResponseType> {
    return this.http.post<ITypelieu>(this.resourceUrl, typelieu, { observe: 'response' });
  }

  update(typelieu: ITypelieu): Observable<EntityResponseType> {
    return this.http.put<ITypelieu>(this.resourceUrl, typelieu, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITypelieu>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITypelieu[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
