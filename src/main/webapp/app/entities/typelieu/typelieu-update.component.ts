import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITypelieu, Typelieu } from 'app/shared/model/typelieu.model';
import { TypelieuService } from './typelieu.service';

@Component({
  selector: 'jhi-typelieu-update',
  templateUrl: './typelieu-update.component.html',
})
export class TypelieuUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    libelle: [null, [Validators.required]],
  });

  constructor(protected typelieuService: TypelieuService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ typelieu }) => {
      this.updateForm(typelieu);
    });
  }

  updateForm(typelieu: ITypelieu): void {
    this.editForm.patchValue({
      id: typelieu.id,
      libelle: typelieu.libelle,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const typelieu = this.createFromForm();
    if (typelieu.id !== undefined) {
      this.subscribeToSaveResponse(this.typelieuService.update(typelieu));
    } else {
      this.subscribeToSaveResponse(this.typelieuService.create(typelieu));
    }
  }

  private createFromForm(): ITypelieu {
    return {
      ...new Typelieu(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITypelieu>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
