import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ExamenSharedModule } from 'app/shared/shared.module';
import { TypelieuComponent } from './typelieu.component';
import { TypelieuDetailComponent } from './typelieu-detail.component';
import { TypelieuUpdateComponent } from './typelieu-update.component';
import { TypelieuDeleteDialogComponent } from './typelieu-delete-dialog.component';
import { typelieuRoute } from './typelieu.route';

@NgModule({
  imports: [ExamenSharedModule, RouterModule.forChild(typelieuRoute)],
  declarations: [TypelieuComponent, TypelieuDetailComponent, TypelieuUpdateComponent, TypelieuDeleteDialogComponent],
  entryComponents: [TypelieuDeleteDialogComponent],
})
export class ExamenTypelieuModule {}
