import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITypelieu } from 'app/shared/model/typelieu.model';

@Component({
  selector: 'jhi-typelieu-detail',
  templateUrl: './typelieu-detail.component.html',
})
export class TypelieuDetailComponent implements OnInit {
  typelieu: ITypelieu | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ typelieu }) => (this.typelieu = typelieu));
  }

  previousState(): void {
    window.history.back();
  }
}
