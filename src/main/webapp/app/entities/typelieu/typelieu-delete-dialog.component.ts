import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITypelieu } from 'app/shared/model/typelieu.model';
import { TypelieuService } from './typelieu.service';

@Component({
  templateUrl: './typelieu-delete-dialog.component.html',
})
export class TypelieuDeleteDialogComponent {
  typelieu?: ITypelieu;

  constructor(protected typelieuService: TypelieuService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.typelieuService.delete(id).subscribe(() => {
      this.eventManager.broadcast('typelieuListModification');
      this.activeModal.close();
    });
  }
}
