package com.isi.jee.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Lieu.
 */
@Entity
@Table(name = "lieu")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Lieu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nomlieu", nullable = false, unique = true)
    private String nomlieu;

    @Column(name = "description")
    private String description;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "email")
    private String email;

    @Column(name = "telephonefixe")
    private String telephonefixe;

    @Column(name = "telephoneportable")
    private String telephoneportable;

    @Column(name = "prix")
    private Integer prix;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @Column(name = "image_content_type")
    private String imageContentType;

    @ManyToOne
    @JsonIgnoreProperties(value = "lieus", allowSetters = true)
    private Typelieu typelieu;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomlieu() {
        return nomlieu;
    }

    public Lieu nomlieu(String nomlieu) {
        this.nomlieu = nomlieu;
        return this;
    }

    public void setNomlieu(String nomlieu) {
        this.nomlieu = nomlieu;
    }

    public String getDescription() {
        return description;
    }

    public Lieu description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdresse() {
        return adresse;
    }

    public Lieu adresse(String adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getEmail() {
        return email;
    }

    public Lieu email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephonefixe() {
        return telephonefixe;
    }

    public Lieu telephonefixe(String telephonefixe) {
        this.telephonefixe = telephonefixe;
        return this;
    }

    public void setTelephonefixe(String telephonefixe) {
        this.telephonefixe = telephonefixe;
    }

    public String getTelephoneportable() {
        return telephoneportable;
    }

    public Lieu telephoneportable(String telephoneportable) {
        this.telephoneportable = telephoneportable;
        return this;
    }

    public void setTelephoneportable(String telephoneportable) {
        this.telephoneportable = telephoneportable;
    }

    public Integer getPrix() {
        return prix;
    }

    public Lieu prix(Integer prix) {
        this.prix = prix;
        return this;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public byte[] getImage() {
        return image;
    }

    public Lieu image(byte[] image) {
        this.image = image;
        return this;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public Lieu imageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
        return this;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public Typelieu getTypelieu() {
        return typelieu;
    }

    public Lieu typelieu(Typelieu typelieu) {
        this.typelieu = typelieu;
        return this;
    }

    public void setTypelieu(Typelieu typelieu) {
        this.typelieu = typelieu;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Lieu)) {
            return false;
        }
        return id != null && id.equals(((Lieu) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Lieu{" +
            "id=" + getId() +
            ", nomlieu='" + getNomlieu() + "'" +
            ", description='" + getDescription() + "'" +
            ", adresse='" + getAdresse() + "'" +
            ", email='" + getEmail() + "'" +
            ", telephonefixe='" + getTelephonefixe() + "'" +
            ", telephoneportable='" + getTelephoneportable() + "'" +
            ", prix=" + getPrix() +
            ", image='" + getImage() + "'" +
            ", imageContentType='" + getImageContentType() + "'" +
            "}";
    }
}
