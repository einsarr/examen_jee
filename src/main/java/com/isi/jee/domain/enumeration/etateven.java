package com.isi.jee.domain.enumeration;

/**
 * The etateven enumeration.
 */
public enum etateven {
    ACTIF, INACTIF
}
