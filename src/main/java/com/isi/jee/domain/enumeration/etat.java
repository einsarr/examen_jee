package com.isi.jee.domain.enumeration;

/**
 * The etat enumeration.
 */
public enum etat {
    ACTIF, INACTIF
}
