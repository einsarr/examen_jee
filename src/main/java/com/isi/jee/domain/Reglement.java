package com.isi.jee.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A Reglement.
 */
@Entity
@Table(name = "reglement")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Reglement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "datereglement")
    private ZonedDateTime datereglement;

    @Column(name = "montant")
    private Long montant;

    @Column(name = "codepaiement")
    private String codepaiement;

    @Column(name = "modereglement")
    private String modereglement;

    @ManyToOne
    @JsonIgnoreProperties(value = "reglements", allowSetters = true)
    private Commande commande;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDatereglement() {
        return datereglement;
    }

    public Reglement datereglement(ZonedDateTime datereglement) {
        this.datereglement = datereglement;
        return this;
    }

    public void setDatereglement(ZonedDateTime datereglement) {
        this.datereglement = datereglement;
    }

    public Long getMontant() {
        return montant;
    }

    public Reglement montant(Long montant) {
        this.montant = montant;
        return this;
    }

    public void setMontant(Long montant) {
        this.montant = montant;
    }

    public String getCodepaiement() {
        return codepaiement;
    }

    public Reglement codepaiement(String codepaiement) {
        this.codepaiement = codepaiement;
        return this;
    }

    public void setCodepaiement(String codepaiement) {
        this.codepaiement = codepaiement;
    }

    public String getModereglement() {
        return modereglement;
    }

    public Reglement modereglement(String modereglement) {
        this.modereglement = modereglement;
        return this;
    }

    public void setModereglement(String modereglement) {
        this.modereglement = modereglement;
    }

    public Commande getCommande() {
        return commande;
    }

    public Reglement commande(Commande commande) {
        this.commande = commande;
        return this;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Reglement)) {
            return false;
        }
        return id != null && id.equals(((Reglement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Reglement{" +
            "id=" + getId() +
            ", datereglement='" + getDatereglement() + "'" +
            ", montant=" + getMontant() +
            ", codepaiement='" + getCodepaiement() + "'" +
            ", modereglement='" + getModereglement() + "'" +
            "}";
    }
}
