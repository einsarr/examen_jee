package com.isi.jee.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Commande.
 */
@Entity
@Table(name = "commande")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Commande implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "datedebut")
    private LocalDate datedebut;

    @Column(name = "datefin")
    private LocalDate datefin;

    @Column(name = "heuredebut")
    private Integer heuredebut;

    @Column(name = "heurefin")
    private Integer heurefin;

    @Column(name = "pointsfidelite")
    private Integer pointsfidelite;

    @ManyToOne
    @JsonIgnoreProperties(value = "commandes", allowSetters = true)
    private Client client;

    @ManyToOne
    @JsonIgnoreProperties(value = "commandes", allowSetters = true)
    private Lieu lieu;

    @ManyToOne
    @JsonIgnoreProperties(value = "commandes", allowSetters = true)
    private Evenement evenement;

    @ManyToOne
    @JsonIgnoreProperties(value = "commandes", allowSetters = true)
    private Fete fete;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDatedebut() {
        return datedebut;
    }

    public Commande datedebut(LocalDate datedebut) {
        this.datedebut = datedebut;
        return this;
    }

    public void setDatedebut(LocalDate datedebut) {
        this.datedebut = datedebut;
    }

    public LocalDate getDatefin() {
        return datefin;
    }

    public Commande datefin(LocalDate datefin) {
        this.datefin = datefin;
        return this;
    }

    public void setDatefin(LocalDate datefin) {
        this.datefin = datefin;
    }

    public Integer getHeuredebut() {
        return heuredebut;
    }

    public Commande heuredebut(Integer heuredebut) {
        this.heuredebut = heuredebut;
        return this;
    }

    public void setHeuredebut(Integer heuredebut) {
        this.heuredebut = heuredebut;
    }

    public Integer getHeurefin() {
        return heurefin;
    }

    public Commande heurefin(Integer heurefin) {
        this.heurefin = heurefin;
        return this;
    }

    public void setHeurefin(Integer heurefin) {
        this.heurefin = heurefin;
    }

    public Integer getPointsfidelite() {
        return pointsfidelite;
    }

    public Commande pointsfidelite(Integer pointsfidelite) {
        this.pointsfidelite = pointsfidelite;
        return this;
    }

    public void setPointsfidelite(Integer pointsfidelite) {
        this.pointsfidelite = pointsfidelite;
    }

    public Client getClient() {
        return client;
    }

    public Commande client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Lieu getLieu() {
        return lieu;
    }

    public Commande lieu(Lieu lieu) {
        this.lieu = lieu;
        return this;
    }

    public void setLieu(Lieu lieu) {
        this.lieu = lieu;
    }

    public Evenement getEvenement() {
        return evenement;
    }

    public Commande evenement(Evenement evenement) {
        this.evenement = evenement;
        return this;
    }

    public void setEvenement(Evenement evenement) {
        this.evenement = evenement;
    }

    public Fete getFete() {
        return fete;
    }

    public Commande fete(Fete fete) {
        this.fete = fete;
        return this;
    }

    public void setFete(Fete fete) {
        this.fete = fete;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Commande)) {
            return false;
        }
        return id != null && id.equals(((Commande) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Commande{" +
            "id=" + getId() +
            ", datedebut='" + getDatedebut() + "'" +
            ", datefin='" + getDatefin() + "'" +
            ", heuredebut=" + getHeuredebut() +
            ", heurefin=" + getHeurefin() +
            ", pointsfidelite=" + getPointsfidelite() +
            "}";
    }
}
