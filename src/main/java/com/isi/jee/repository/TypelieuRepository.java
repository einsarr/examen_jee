package com.isi.jee.repository;

import com.isi.jee.domain.Typelieu;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Typelieu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypelieuRepository extends JpaRepository<Typelieu, Long> {
}
