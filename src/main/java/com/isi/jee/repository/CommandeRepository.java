package com.isi.jee.repository;

import com.isi.jee.domain.Client;
import com.isi.jee.domain.Commande;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Commande entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommandeRepository extends JpaRepository<Commande, Long> {

    List<Commande> findCommandeByClient(Client client);
}
