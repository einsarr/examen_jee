package com.isi.jee.repository;

import com.isi.jee.domain.Fete;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Fete entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FeteRepository extends JpaRepository<Fete, Long> {
}
