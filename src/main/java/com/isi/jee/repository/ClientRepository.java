package com.isi.jee.repository;

import com.isi.jee.domain.Client;

import com.isi.jee.domain.User;
import org.springframework.data.jpa.repository.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Client entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query(value = "SELECT c FROM Client c JOIN User u  on u.id=c.user.id WHERE u.login=?1")
    Client findClientByUser(String login);


}
