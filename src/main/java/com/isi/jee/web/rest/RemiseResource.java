package com.isi.jee.web.rest;

import com.isi.jee.domain.Remise;
import com.isi.jee.service.RemiseService;
import com.isi.jee.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.isi.jee.domain.Remise}.
 */
@RestController
@RequestMapping("/api")
public class RemiseResource {

    private final Logger log = LoggerFactory.getLogger(RemiseResource.class);

    private static final String ENTITY_NAME = "remise";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RemiseService remiseService;

    public RemiseResource(RemiseService remiseService) {
        this.remiseService = remiseService;
    }

    /**
     * {@code POST  /remises} : Create a new remise.
     *
     * @param remise the remise to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new remise, or with status {@code 400 (Bad Request)} if the remise has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/remises")
    public ResponseEntity<Remise> createRemise(@RequestBody Remise remise) throws URISyntaxException {
        log.debug("REST request to save Remise : {}", remise);
        if (remise.getId() != null) {
            throw new BadRequestAlertException("A new remise cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Remise result = remiseService.save(remise);
        return ResponseEntity.created(new URI("/api/remises/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /remises} : Updates an existing remise.
     *
     * @param remise the remise to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated remise,
     * or with status {@code 400 (Bad Request)} if the remise is not valid,
     * or with status {@code 500 (Internal Server Error)} if the remise couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/remises")
    public ResponseEntity<Remise> updateRemise(@RequestBody Remise remise) throws URISyntaxException {
        log.debug("REST request to update Remise : {}", remise);
        if (remise.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Remise result = remiseService.save(remise);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, remise.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /remises} : get all the remises.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of remises in body.
     */
    @GetMapping("/remises")
    public ResponseEntity<List<Remise>> getAllRemises(Pageable pageable) {
        log.debug("REST request to get a page of Remises");
        Page<Remise> page = remiseService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /remises/:id} : get the "id" remise.
     *
     * @param id the id of the remise to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the remise, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/remises/{id}")
    public ResponseEntity<Remise> getRemise(@PathVariable Long id) {
        log.debug("REST request to get Remise : {}", id);
        Optional<Remise> remise = remiseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(remise);
    }

    /**
     * {@code DELETE  /remises/:id} : delete the "id" remise.
     *
     * @param id the id of the remise to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/remises/{id}")
    public ResponseEntity<Void> deleteRemise(@PathVariable Long id) {
        log.debug("REST request to delete Remise : {}", id);
        remiseService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
