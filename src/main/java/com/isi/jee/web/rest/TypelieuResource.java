package com.isi.jee.web.rest;

import com.isi.jee.domain.Typelieu;
import com.isi.jee.service.TypelieuService;
import com.isi.jee.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.isi.jee.domain.Typelieu}.
 */
@RestController
@RequestMapping("/api")
public class TypelieuResource {

    private final Logger log = LoggerFactory.getLogger(TypelieuResource.class);

    private static final String ENTITY_NAME = "typelieu";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TypelieuService typelieuService;

    public TypelieuResource(TypelieuService typelieuService) {
        this.typelieuService = typelieuService;
    }

    /**
     * {@code POST  /typelieus} : Create a new typelieu.
     *
     * @param typelieu the typelieu to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new typelieu, or with status {@code 400 (Bad Request)} if the typelieu has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/typelieus")
    public ResponseEntity<Typelieu> createTypelieu(@Valid @RequestBody Typelieu typelieu) throws URISyntaxException {
        log.debug("REST request to save Typelieu : {}", typelieu);
        if (typelieu.getId() != null) {
            throw new BadRequestAlertException("A new typelieu cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Typelieu result = typelieuService.save(typelieu);
        return ResponseEntity.created(new URI("/api/typelieus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /typelieus} : Updates an existing typelieu.
     *
     * @param typelieu the typelieu to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated typelieu,
     * or with status {@code 400 (Bad Request)} if the typelieu is not valid,
     * or with status {@code 500 (Internal Server Error)} if the typelieu couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/typelieus")
    public ResponseEntity<Typelieu> updateTypelieu(@Valid @RequestBody Typelieu typelieu) throws URISyntaxException {
        log.debug("REST request to update Typelieu : {}", typelieu);
        if (typelieu.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Typelieu result = typelieuService.save(typelieu);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, typelieu.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /typelieus} : get all the typelieus.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of typelieus in body.
     */
    @GetMapping("/typelieus")
    public ResponseEntity<List<Typelieu>> getAllTypelieus(Pageable pageable) {
        log.debug("REST request to get a page of Typelieus");
        Page<Typelieu> page = typelieuService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /typelieus/:id} : get the "id" typelieu.
     *
     * @param id the id of the typelieu to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the typelieu, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/typelieus/{id}")
    public ResponseEntity<Typelieu> getTypelieu(@PathVariable Long id) {
        log.debug("REST request to get Typelieu : {}", id);
        Optional<Typelieu> typelieu = typelieuService.findOne(id);
        return ResponseUtil.wrapOrNotFound(typelieu);
    }

    /**
     * {@code DELETE  /typelieus/:id} : delete the "id" typelieu.
     *
     * @param id the id of the typelieu to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/typelieus/{id}")
    public ResponseEntity<Void> deleteTypelieu(@PathVariable Long id) {
        log.debug("REST request to delete Typelieu : {}", id);
        typelieuService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
