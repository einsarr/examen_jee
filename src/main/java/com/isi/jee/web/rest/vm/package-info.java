/**
 * View Models used by Spring MVC REST controllers.
 */
package com.isi.jee.web.rest.vm;
