package com.isi.jee.web.rest;

import com.isi.jee.domain.Fete;
import com.isi.jee.service.FeteService;
import com.isi.jee.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.isi.jee.domain.Fete}.
 */
@RestController
@RequestMapping("/api")
public class FeteResource {

    private final Logger log = LoggerFactory.getLogger(FeteResource.class);

    private static final String ENTITY_NAME = "fete";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FeteService feteService;

    public FeteResource(FeteService feteService) {
        this.feteService = feteService;
    }

    /**
     * {@code POST  /fetes} : Create a new fete.
     *
     * @param fete the fete to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fete, or with status {@code 400 (Bad Request)} if the fete has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fetes")
    public ResponseEntity<Fete> createFete(@RequestBody Fete fete) throws URISyntaxException {
        log.debug("REST request to save Fete : {}", fete);
        if (fete.getId() != null) {
            throw new BadRequestAlertException("A new fete cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Fete result = feteService.save(fete);
        return ResponseEntity.created(new URI("/api/fetes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fetes} : Updates an existing fete.
     *
     * @param fete the fete to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fete,
     * or with status {@code 400 (Bad Request)} if the fete is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fete couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fetes")
    public ResponseEntity<Fete> updateFete(@RequestBody Fete fete) throws URISyntaxException {
        log.debug("REST request to update Fete : {}", fete);
        if (fete.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Fete result = feteService.save(fete);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, fete.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fetes} : get all the fetes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fetes in body.
     */
    @GetMapping("/fetes")
    public ResponseEntity<List<Fete>> getAllFetes(Pageable pageable) {
        log.debug("REST request to get a page of Fetes");
        Page<Fete> page = feteService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /fetes/:id} : get the "id" fete.
     *
     * @param id the id of the fete to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fete, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fetes/{id}")
    public ResponseEntity<Fete> getFete(@PathVariable Long id) {
        log.debug("REST request to get Fete : {}", id);
        Optional<Fete> fete = feteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(fete);
    }

    /**
     * {@code DELETE  /fetes/:id} : delete the "id" fete.
     *
     * @param id the id of the fete to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fetes/{id}")
    public ResponseEntity<Void> deleteFete(@PathVariable Long id) {
        log.debug("REST request to delete Fete : {}", id);
        feteService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
