package com.isi.jee.service;

import com.isi.jee.domain.Fete;
import com.isi.jee.repository.FeteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Fete}.
 */
@Service
@Transactional
public class FeteService {

    private final Logger log = LoggerFactory.getLogger(FeteService.class);

    private final FeteRepository feteRepository;

    public FeteService(FeteRepository feteRepository) {
        this.feteRepository = feteRepository;
    }

    /**
     * Save a fete.
     *
     * @param fete the entity to save.
     * @return the persisted entity.
     */
    public Fete save(Fete fete) {
        log.debug("Request to save Fete : {}", fete);
        return feteRepository.save(fete);
    }

    /**
     * Get all the fetes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Fete> findAll(Pageable pageable) {
        log.debug("Request to get all Fetes");
        return feteRepository.findAll(pageable);
    }


    /**
     * Get one fete by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Fete> findOne(Long id) {
        log.debug("Request to get Fete : {}", id);
        return feteRepository.findById(id);
    }

    /**
     * Delete the fete by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Fete : {}", id);
        feteRepository.deleteById(id);
    }
}
