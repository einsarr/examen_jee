package com.isi.jee.service;

import com.isi.jee.domain.Typelieu;
import com.isi.jee.repository.TypelieuRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Typelieu}.
 */
@Service
@Transactional
public class TypelieuService {

    private final Logger log = LoggerFactory.getLogger(TypelieuService.class);

    private final TypelieuRepository typelieuRepository;

    public TypelieuService(TypelieuRepository typelieuRepository) {
        this.typelieuRepository = typelieuRepository;
    }

    /**
     * Save a typelieu.
     *
     * @param typelieu the entity to save.
     * @return the persisted entity.
     */
    public Typelieu save(Typelieu typelieu) {
        log.debug("Request to save Typelieu : {}", typelieu);
        return typelieuRepository.save(typelieu);
    }

    /**
     * Get all the typelieus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Typelieu> findAll(Pageable pageable) {
        log.debug("Request to get all Typelieus");
        return typelieuRepository.findAll(pageable);
    }


    /**
     * Get one typelieu by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Typelieu> findOne(Long id) {
        log.debug("Request to get Typelieu : {}", id);
        return typelieuRepository.findById(id);
    }

    /**
     * Delete the typelieu by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Typelieu : {}", id);
        typelieuRepository.deleteById(id);
    }
}
