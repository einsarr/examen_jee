package com.isi.jee.service;

import com.isi.jee.domain.Reglement;
import com.isi.jee.repository.ReglementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Reglement}.
 */
@Service
@Transactional
public class ReglementService {

    private final Logger log = LoggerFactory.getLogger(ReglementService.class);

    private final ReglementRepository reglementRepository;

    public ReglementService(ReglementRepository reglementRepository) {
        this.reglementRepository = reglementRepository;
    }

    /**
     * Save a reglement.
     *
     * @param reglement the entity to save.
     * @return the persisted entity.
     */
    public Reglement save(Reglement reglement) {
        log.debug("Request to save Reglement : {}", reglement);
        return reglementRepository.save(reglement);
    }

    /**
     * Get all the reglements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Reglement> findAll(Pageable pageable) {
        log.debug("Request to get all Reglements");
        return reglementRepository.findAll(pageable);
    }


    /**
     * Get one reglement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Reglement> findOne(Long id) {
        log.debug("Request to get Reglement : {}", id);
        return reglementRepository.findById(id);
    }

    /**
     * Delete the reglement by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Reglement : {}", id);
        reglementRepository.deleteById(id);
    }
}
