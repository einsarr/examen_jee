package com.isi.jee.service;

import com.isi.jee.domain.Remise;
import com.isi.jee.repository.RemiseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Remise}.
 */
@Service
@Transactional
public class RemiseService {

    private final Logger log = LoggerFactory.getLogger(RemiseService.class);

    private final RemiseRepository remiseRepository;

    public RemiseService(RemiseRepository remiseRepository) {
        this.remiseRepository = remiseRepository;
    }

    /**
     * Save a remise.
     *
     * @param remise the entity to save.
     * @return the persisted entity.
     */
    public Remise save(Remise remise) {
        log.debug("Request to save Remise : {}", remise);
        return remiseRepository.save(remise);
    }

    /**
     * Get all the remises.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Remise> findAll(Pageable pageable) {
        log.debug("Request to get all Remises");
        return remiseRepository.findAll(pageable);
    }


    /**
     * Get one remise by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Remise> findOne(Long id) {
        log.debug("Request to get Remise : {}", id);
        return remiseRepository.findById(id);
    }

    /**
     * Delete the remise by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Remise : {}", id);
        remiseRepository.deleteById(id);
    }
}
