package com.isi.jee.service;

import com.isi.jee.domain.Lieu;
import com.isi.jee.repository.LieuRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Lieu}.
 */
@Service
@Transactional
public class LieuService {

    private final Logger log = LoggerFactory.getLogger(LieuService.class);

    private final LieuRepository lieuRepository;

    public LieuService(LieuRepository lieuRepository) {
        this.lieuRepository = lieuRepository;
    }

    /**
     * Save a lieu.
     *
     * @param lieu the entity to save.
     * @return the persisted entity.
     */
    public Lieu save(Lieu lieu) {
        log.debug("Request to save Lieu : {}", lieu);
        return lieuRepository.save(lieu);
    }

    /**
     * Get all the lieus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Lieu> findAll(Pageable pageable) {
        log.debug("Request to get all Lieus");
        return lieuRepository.findAll(pageable);
    }


    /**
     * Get one lieu by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Lieu> findOne(Long id) {
        log.debug("Request to get Lieu : {}", id);
        return lieuRepository.findById(id);
    }

    /**
     * Delete the lieu by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Lieu : {}", id);
        lieuRepository.deleteById(id);
    }
}
